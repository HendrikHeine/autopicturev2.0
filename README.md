## AutoPicture v2 Python
AutoPicture v2 ist die zweite Version, nur diesesmal in Python :)\
Es sortiert eine große menge Bilder in kurzer Zeit nach Datum und verschiebt sie in die passenden Ordner. Die Ordnerstrukur sieht so aus:
- Bilder
    - 2020
        - 08
            - 06\
            001.jpg\
            002.jpg\
            003.jpg
            - 09\
            001.jpg\
            002.jpg\
            003.jpg
    - 2021
        - 10
            - 28\
            001.jpg\
            002.jpg\
            003.jpg
        - 11
            - 10\
            001.jpg\
            002.jpg\
            003.jpg
            - 29\
            001.jpg\
            002.jpg\
            003.jpg

Für genaue Informationen und: https://t.me/autopictureDevelopmentNews 

* Wichtiger Hinweis:\
    Das Programm ist noch in der Development Phase. Es kann zu Fehlfunktionen und/oder Datenverlust kommen. Bitte erstmal mit Kopien von Bildern arbeiten. Wenn alles funktioniert, kann mit den Originalen gearbeitet werden. Ich übernehme keine Verantwortung im Falle von Datenverlust.
##   Bisher unterstützte und getestete Betriebssysteme:
    Linux
        Mint 20.1 / 20.2 64bit

    Windows
        10 64bit
#
##   Theoretisch kompatible Betriebssysteme:
    Linux
        Debian
        Ubuntu
    
    MacOs

    Windows
        XP/7/8/8.1
#

## Abhängigkeiten
*   Python:\
    Bei Windows muss Python manuell installiert werden\
    https://www.python.org/ftp/python/3.9.6/python-3.9.6-amd64.exe (Direktdownload 64bit) \
    https://www.python.org/ftp/python/3.9.6/python-3.9.6.exe (Direktdownload 32bit) \
    https://www.python.org/downloads/windows/ (Download Seite für andere Versionen) \
    Bei Linux ist Python in der Regel schon vorinstalliert. Falls nicht, wird dies vom Installer automatisch nachgeholt.
 
*   PIP:\
    In Linux wird eine virtuelle Pythonumgebung erzeugt, damit es keine Konflikte im internen Python gibt. Folgene Libs werden via PIP installiert:
    -   pillow
    -   progressbar

## Installation
*   Linux:
    1. Rechtsklick im Verzeichnis von AutoPicture und "im Terminal öffnen" auswählen
    2. Den Befehl ./installLinux.sh eingeben
    3. Sudo Passwort eingeben, damit die aktuellste Version von Python installiert werden kann
    4. Quelle eingeben. Die Quelle ist das Verzeichnis, indem sich die Bilder befinden, die sortiert werden sollen.
    5. Ziel eingeben. Dies ist das Verzeichnis, in das die Bilder sortiert
    werden. 
    6. Nun können die Eingaben kontrolliert werden und falls nötig, korrigiert werden.
    7. Jetzt wird AutoPicture installiert.
    8. Mit der Datei `AutoPicture.sh` wird das Programm gestartet. Ohne diesen Dateiaufruf kann es zu Problemen mit den Python Libs kommen. Daher immer zum Starten diese Datei benutzen.
#
*   MacOS: \
    Ich habe leider keine Möglichkeit, mein Programm unter MacOS zu testen, daher ist die Installation unter Mac auf eigene Gefahr... falls es jemand mal ausprobieren sollte, würde ich mich sehr über infos interessieren. eMail ist in meinem GitLab Account :)
#
*   Windows:
    1. Rechtsklick auf die Datei "installWindows"
    2. Als Administrator ausführen
    3. Quelle eingeben. Die Quelle ist das Verzeichnis, indem sich die Bilder befinden, die sortiert werden sollen.
    4. Ziel eingeben. Dies ist das Verzeichnis, in das die Bilder sortiert
    werden.
    5. Nun können die Eingaben kontrolliert werden und falls nötig, korregiert werden.
    6. Jetzt wird AutoPicture installiert.
