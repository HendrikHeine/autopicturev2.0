import datetime

class logging:
    def __init__(self):
        self.logData = []

    def log(self, quelle, ziel, dataName):
        dateNow = datetime.datetime.now()
        dateNow = str(dateNow).split(" ")
        time = dateNow[1]
        time = time.split(".")
        time = time[0]
        dateNow = dateNow[0]

        #dateNow: year-month-day
        #time: hour:minute:second

        self.logData.append(f"{dateNow};{time};{quelle};{ziel};{dataName}")
