import os
import updater
import getMeta
import progressbar
import sortPic as sortP
import prozent as calcPro
import sqliteHandler as sql

os.chdir("..")
connection = sql.sql("database.sqlite")
os.chdir("src")
config = connection.getConfig()

workingDir = config[0]
homeDir = config[1]
user = config[2]
operationSystem = config[3]
quelle = config[4]
ziel = config[5]
appVersion = config[6]
date = config[7]
slash = config[8]
clsCommand = config[9]

os.system(clsCommand)

print("")
print(f"+---------------------------<|AutoPicture|>------------------------------")
print(f"|")
print(f"+ App Version v{appVersion} vom {date}")
print(f"|")
print(f"+------------------------------------------------------------------------")
print("")

update = updater.update(appVersion)
update.checkUpdate()




try:
    meta = getMeta.getMetaData(quelle, slash)
    sortPic = sortP.sortPic(quelle, ziel, slash, operationSystem)
    sortPic.check()
    numberIfPics = meta.countPics()
    prozent = calcPro.pp()
    terminalSize = os.get_terminal_size()

    if numberIfPics >= 0:
        count = 0
        print(f"\n{numberIfPics + 1} Datei(en) werden sortiert...\n")
        pgBar = progressbar.progressbar.ProgressBar().start()
        while count <= numberIfPics:
            metaD = meta.getData()
            pro = prozent.pro(numberIfPics, count)
            pro = int(pro)
            pgBar.update(pro)
            sortPic.sort(meta.getData())
            count += 1

        connection.logging(sortPic.logForDatabase.logData)
        print("")
        print(f"Erfolgreich sortierte Bilder: {sortPic.countNormalSorts}")
        print(f"Videos: {sortPic.countFailedVideo}")
        print(f"Kaputte Dateien: {sortPic.countFailedPics}")
        print("")
        if operationSystem == "Linux":
            os.system(f'notify-send "AutoPicture" "fertig!"')
            
    else:
        print("Keine Dateien im Ordner")
finally:
    connection.close()
    