# Return 0 = Error
# Return 1 = Video error

import os
from PIL import Image, ExifTags


class getMetaData:
    def __init__(self, tempPicPath, slash):
        self.__tempPicPath = tempPicPath
        self.__listFromFolder = ""
        self.__listDir = type(list())
        self.__slash = slash
    
    def __scanFolder(self):
        self.__listDir = os.listdir(self.__tempPicPath)

    def __filterData(self, value, keyWords):
        valueReturn = []
        for k in keyWords:
            for v in value:
                temp = v.split("|")
                if temp[0] == k:
                    valueReturn.append(v)
        return valueReturn

    def __fotmatDate(self, io):
        for i in io:
            i = i.split("|")
            if i[0] == "DateTimeOriginal":
                date = i[1].split(" ")
                return date[0]
    
    def __formatMake(self, io):
        for i in io:
            i = i.split("|")
            if i[0] == "Make":
                make = i[1].split(" ")
                return make[0]

    def countPics(self):
        self.__scanFolder()
        return (len(self.__listDir)) -1

    def getData(self):
        value = []
        numberOfPics = self.countPics()
        valueInFolder = self.__listDir
        
        keyWords = [ "DateTimeOriginal", "Make" ]
        dataName = valueInFolder[0].split(".")
        videoFormats = [ "MP4", "MOV", "M4V", "MKV", "AVI", "WMV", "AVCHD", "WEBM", "MPEG" ]
        for format in videoFormats:
            if dataName[1] == format or dataName[1] == format.lower():
                returnValue = [ 1, valueInFolder[0] ]
                return returnValue
        
        try:
            img = Image.open(f"{self.__tempPicPath}{self.__slash}{valueInFolder[0]}")
            for i,j in img._getexif().items():
                if i in ExifTags.TAGS:
                    value.append(ExifTags.TAGS[i] + "|" + str(j))
            value = self.__filterData(value, keyWords)
            date = self.__fotmatDate(value)
            make = self.__formatMake(value)
            returnValue = [ date, make, valueInFolder[0] ]
        except:
            returnValue = [ 2, valueInFolder[0] ]
        if returnValue[0] == None:
            returnValue = [ 2, valueInFolder[0] ]
            return returnValue
        else:
            return returnValue
