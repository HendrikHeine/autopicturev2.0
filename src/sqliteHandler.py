import sqlite3 as sqlData
import formatSQLdata

class sql:
    def __init__(self, databaseName):
        self.__databasePath = databaseName
        self.__connection = sqlData.connect(self.__databasePath)
        self.__cursor = self.__connection.cursor()

    def getConfig(self):
        self.__cursor.execute("SELECT * FROM config;")
        rows = self.__cursor.fetchall()
        formatSQL = formatSQLdata.format(rows)
        value = formatSQL.getValue()
        return value

    def install(self, workingDir, homeDir, user, opS, quelle, ziel, version,date, slash, clsCommand):
        self.__cursor.execute("DROP TABLE IF EXISTS config;")
        self.__cursor.execute("DROP TABLE IF EXISTS log;")

        value = "workingDir STRING, homeDir STRING, user STRING, os STRING, quelle STRING, ziel STRING, version STRING, date DATE, slash STRING, clsCommand STRING"
        self.createTable(value, "config")

        value = "id INTEGER PRIMARY KEY AUTOINCREMENT, date DATE, time DATETIME, quelle STRING, ziel STRING, dataName STRING"
        self.createTable(value, "log")
        
        data = (workingDir, homeDir, user, opS, quelle, ziel, version, date, slash, clsCommand)
        query = """INSERT INTO config (workingDir, homeDir, user, os, quelle, ziel, version, date, slash, clsCommand) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);"""
        self.__cursor.execute(query, data)
        self.__connection.commit()

    def update(self, version, date):
        self.__cursor.execute(f'UPDATE config SET version = "{version}";')
        self.__cursor.execute(f'UPDATE config SET date = "{date}";')
        self.__connection.commit()

    def logging(self, logs):
        for i in logs:
            value = str(i).split(";")
            data = (value[0], value[1], value[2], value[3], value[4])
            query = """INSERT INTO log (date, time, quelle, ziel, dataName) VALUES (?, ?, ?, ?, ?);"""
            self.__cursor.execute(query, data)
        self.__connection.commit()

    def editDB_insert(self, table, row, value):
        data = (value)
        query = f"""INSERT INTO {table} ({row}) VALUES (?);"""
        self.__cursor.execute(query, data)
        self.__connection.commit()

    def countRows(self, row):
        self.__cursor.execute(f"SELECT COUNT() FROM {row};")
        rows = self.__cursor.fetchall()
        formatSQL = formatSQLdata.format(rows)
        value = formatSQL.getValueForRowCount()
        return value

    def createTable(self, tableValue, tableName):
        self.__cursor.execute(f"CREATE TABLE {tableName} ({tableValue});")
        self.__connection.commit()

    def close(self):
        self.__cursor.close()
        self.__connection.close()
