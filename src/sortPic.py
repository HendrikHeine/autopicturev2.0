import os
import log

class sortPic:
    def __init__(self, tempPicPath, picFolder, slash, operationSystem):
        self.logForDatabase = log.logging()
        self.__operationSystem = operationSystem
        self.__brand = ""
        self.__picName = ""
        self.__day = ""
        self.__month = ""
        self.__year = ""
        self.__fullPath = ""
        self.__finalPath = ""
        self.__quelle = tempPicPath
        self.__ziel = picFolder
        self.__slash = slash
        self.countNormalSorts = 0
        self.countFailedVideo = 0
        self.countFailedPics = 0

    def __errorCase(self, exifData):
        if exifData[0] == 1:
            if not os.path.exists(f"{self.__ziel}{self.__slash}videos"):
                os.mkdir(f"{self.__ziel}{self.__slash}videos")
            self.logForDatabase.log(self.__quelle, f"{self.__ziel}{self.__slash}videos", exifData[1])
            os.replace(f"{self.__quelle}{self.__slash}{exifData[1]}", f"{self.__ziel}{self.__slash}videos{self.__slash}{exifData[1]}")
            self.countFailedVideo = self.countFailedVideo +1
        if exifData[0] == 2:
            if not os.path.exists(f"{self.__ziel}{self.__slash}Ohne_Datum"):
                os.mkdir(f"{self.__ziel}{self.__slash}Ohne_Datum")
            self.logForDatabase.log(self.__quelle, f"{self.__ziel}{self.__slash}Ohne_Datum", exifData[1])
            os.replace(f"{self.__quelle}{self.__slash}{exifData[1]}", f"{self.__ziel}{self.__slash}Ohne_Datum{self.__slash}{exifData[1]}")
            self.countFailedPics = self.countFailedPics +1

    def __prepareExif(self, exifData):
        try:
            self.__brand = ""
            self.__picName = ""
            self.__day = ""
            self.__month = ""
            self.__year = ""
            self.__fullPath = ""
            self.__finalPath = ""
            lenOfExif = len(exifData)
            if lenOfExif == 2:
                self.__errorCase(exifData)
                return 0
            else:
                date = exifData[0]
                date = date.split(":")
                self.__brand = exifData[1].upper()
                self.__picName = exifData[2]
                self.__day = date[2]
                self.__month = date[1]
                self.__year = date[0]
                self.__fullPath = f"{self.__quelle}{self.__slash}{self.__picName}"
                self.__finalPath = f"{self.__ziel}{self.__slash}{self.__year}{self.__slash}{self.__month}{self.__slash}{self.__day}"
        except:
            pass
    
    def __createFolder(self):
        if not os.path.exists(f"{self.__finalPath}"):
            if self.__operationSystem == "Linux" or self.__operationSystem == "Mac":
                os.system(f"mkdir -p {self.__finalPath}")
            else:
                os.system(f"mkdir {self.__finalPath}")
    
    def check(self):
        if not os.path.exists(self.__ziel):
            print("Ziel fehlt. Wird neu angelegt...")
            if self.__operationSystem == "Linux" or self.__operationSystem == "Mac":
                os.system(f"mkdir -p {self.__ziel}")
            else:
                os.system(f"mkdir {self.__ziel}")
            
        if not os.path.exists(self.__quelle):
            print("Quelle fehlt. Wird neu angelegt...")
            if self.__operationSystem == "Linux" or self.__operationSystem == "Mac":
                os.system(f"mkdir -p {self.__quelle}")
            else:
                os.system(f"mkdir {self.__quelle}")

    def sort(self, exifData):
        value = self.__prepareExif(exifData)
        if value != 0:
            self.__createFolder()
            os.replace(self.__fullPath, f"{self.__finalPath}{self.__slash}{self.__picName}")
            self.logForDatabase.log(self.__quelle, self.__finalPath, self.__picName)
            self.countNormalSorts = self.countNormalSorts +1
        