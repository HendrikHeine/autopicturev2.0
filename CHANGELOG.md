## v2.0.13 | 30.08.2021
*   Modified the installer for Beta versions
*   Add stats about the results after move all pics

## v2.0.12 | 28.08.2021
*   Program does not chrash, when a pic has no datestamp

## v2.0.11 | 27.08.2021
*   The installer is sendig a log status to the Server for statisics (code from Server in the `logServer` Branch)

## v2.0.10 | 19.08.2021
*   Typos in the installation
